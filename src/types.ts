
export type RateArea = string;

export enum PlanLevel {
  bronze = 'bronze',
  silver = 'silver',
  gold = 'gold',
  platinum = 'platinum',
  catastrophic = 'catastrophic'
}

export type RatesByArea = { [key: string]: Set<number> };

export type AreasByZip = { [key: string]: Set<RateArea> };
