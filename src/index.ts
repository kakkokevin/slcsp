#!/usr/bin/env node

import { stringify } from 'csv-stringify';
import { readPlans, readZips, readSlcsp } from './Files';
import { Plan, Zip, ascending } from './Utils';
import { RatesByArea, AreasByZip, PlanLevel } from './types';
//
let DEBUG = false;
let silverRatesByArea: RatesByArea = {};
let areasByZip: AreasByZip = {};

/**
 * For a set of zip codes (provided in slcsp.zip), determines and outputs the
 * rate of the second lowest cost silver plan for each, if one exists.
 */
async function main() {
    silverRatesByArea = await getAreaRatesFromPlans();
    areasByZip = await getAreasByZip();
    
    // Go through the target zip codes and get the SLCSP where applicable
    let { columns, records: targets } = await readSlcsp();
    let slcspsResults = [columns];
    for (let [zip,] of targets) {
        let rate = getSlscpForZip(zip);
        slcspsResults.push([zip, rate]);
    }

    // Output the results as CSV to the console
    stringify(slcspsResults, (err, output) => {
        console.log(output);
    });
}

/**
 * Filters the plans to just those of silver-level, and maps their rates by RateArea.
 */
async function getAreaRatesFromPlans(): Promise<RatesByArea> {
    let silverRatesByArea: RatesByArea = {};
    
    for (let planRecord of (await readPlans()).records) {
        let plan = new Plan(planRecord);
        if (plan.level === PlanLevel.silver) {
            silverRatesByArea[plan.rateArea]
                ? silverRatesByArea[plan.rateArea].add(plan.rate)
                : silverRatesByArea[plan.rateArea] = new Set([plan.rate]);
        }
    }

    return silverRatesByArea;
}

/**
 * Goes through all the possible `zips.csv` records and condenses them
 * to a mapping of RateAreas per zip code.
 */
async function getAreasByZip(): Promise<AreasByZip> {
    let areasByZip = {};
    
    for (let zipRecord of (await readZips()).records) {
        let zip = new Zip(zipRecord);
        if (areasByZip[zip.code]) {
            areasByZip[zip.code].add(zip.rateArea);
        } else {
            areasByZip[zip.code] = new Set([zip.rateArea]);
        }
    }

    return areasByZip;
}

/**
 * Determines the Second-Lowest-Cost-Silver-Plan rate for the given zipcode, if there is one.
 * @param zip 
 * @returns The rate, formatted to 2 decimal places, or an empty string
 */
function getSlscpForZip(zip: string): string {
    // If a zip code isn't in a single area, its SLCSP is ambiguous, and we can't determine it
    let areas = areasByZip[zip];
    if (areas.size !== 1) {
        DEBUG && console.log(`Found ${areas.size} areas for ${zip}, slcsp is ambiguous`);
        return '';
    }
    
    let area = [...areas][0];
    let rates = silverRatesByArea[area];
    // If there are <2 rates for the area, then there's no _second_ lowest rate
    if (!rates || rates.size < 2) {
        DEBUG && console.log(`Found ${rates ? rates.size : 'no'} rates for ${zip}, slcsp is undefined`);
        return '';
    }

    // If we have only 1 area, and >1 rate in that area, we can figure out the slcsp!
    return Array.from(rates).sort(ascending)[1].toFixed(2);
}


try {
    // DEBUG = true; // Uncomment this to log reasons for zips with no rate
    main();
} catch (error) {
    console.error(`Exited early with error: ${error}`);
}