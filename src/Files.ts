import path from 'path';
import { promises as fs } from 'fs';
import { parse } from 'csv-parse';
import Encoding from 'encoding-japanese';

// [plan_id, state, metal_level, rate, rate_area]
type PlansRecord = [string, string, string, string, string];
// [zipcode, state, county_code, name, rate_area]
type ZipsRecord = [string, string, string, string, string];
// [zipcode, rate]
type SlcspRecord = [string, string];
type CSVReadResponse<T> = {
  columns: T;
  records: T[];
}

export async function readPlans(): Promise<CSVReadResponse<PlansRecord>> {
  let [columns, ...records] = await parseCSVFile(path.resolve(__dirname, '../plans.csv'));
  return { columns, records } as CSVReadResponse<PlansRecord>;
}

export async function readZips(): Promise<CSVReadResponse<ZipsRecord>> {
  let [columns, ...records] = await parseCSVFile(path.resolve(__dirname, '../zips.csv'));
  return { columns, records } as CSVReadResponse<ZipsRecord>;
}

export async function readSlcsp(): Promise<CSVReadResponse<SlcspRecord>> {
  let [columns, ...records] = await parseCSVFile(path.resolve(__dirname, '../slcsp.csv'));
  return { columns, records } as CSVReadResponse<SlcspRecord>;
}

/**
* Reads, parses, and returns the contents of a given CSV file
* @param {string} file Path to CSV file
* @returns {Promise<string[][]>}
*/
async function parseCSVFile(file: string): Promise<string[][]> {
  // Make sure the file is a file
  let stats = await fs.stat(file);
  if (!stats.isFile()) {
      throw new Error(`${file} is not a file.`);
  }

  // Attempt to use the correct encoding, defaulting to UTF-8
  let buffer = await fs.readFile(file);
  let encoding = Encoding.detect(buffer) || 'utf8';
  let csvString = buffer.toString(encoding);

  // Parse the csvString to records, and return them
  return new Promise((resolve, reject) => {
      parse(csvString, (err, records: string[][]) => { err ? reject(err) : resolve(records); });
  });
}


module.exports = {
  readPlans,
  readZips,
  readSlcsp
};