import { RateArea, PlanLevel } from "./types";

/**
 * Represents a single record from `plans.csv`, pared down to the values we care about
 */
export class Plan {
  id: string;
  rateArea: RateArea;
  level: PlanLevel;
  rate: number;  

  constructor([id, state, level, rate, area]) {
      this.id = id;
      this.rateArea = getRateArea(state, area);
      this.level = level.toLowerCase();
      this.rate = parseFloat(rate);
  }
}

/**
* Represents a single record from `zips.csv`, pared down to the values we care about
*/
export class Zip {
  code: string;
  rateArea: RateArea;

  constructor([code, state, countyCode, countyName, area]) {
      this.code = code;
      this.rateArea = getRateArea(state, area);
  }
}

/**
 * Sorts items ascending when used as the function passed to Array.prototype.sort
 * @param a 
 * @param b 
 * @returns The difference between _a_ and _b_
 */
export function ascending(a: number, b: number): number { return a - b; }

/** 
* Given a state and area number, returns a standardized string for easier handling of the RateArea tuple.
*/
function getRateArea(state: string, area: string): RateArea { return `${state}:${area}`; }